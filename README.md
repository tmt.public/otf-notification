# OTF Notification Project

#### Technical Stack
- Typescript
- Serverless
- AWS Services
- Unit test (comming soon)

### Components
- Health check api
- Swagger
- Email notification (migrate soon)
- Websocket
- Integrate with Jenkins to notify latest build
