import { ApiCallback, ApiContext, ApiEvent, ApiHandler } from '../../common/api.interfaces';
import { ResponseBuilder } from '../../common/response-builder';
import { GetHealthCheckResult } from './health.interfaces';

export class HealthHandler {
  public getHealthCheck: ApiHandler = (event: ApiEvent, context: ApiContext, callback: ApiCallback): void => {
    const result: GetHealthCheckResult = {
      success: true
    };

    ResponseBuilder.ok<GetHealthCheckResult>(result, callback);
  }
}
