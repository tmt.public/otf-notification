import { ApiHandler } from '../../common/api.interfaces';
import { HealthHandler } from './health.handler';

const handler: HealthHandler = new HealthHandler();

export const getHealthCheck: ApiHandler = handler.getHealthCheck;
