import { APIGateway } from 'aws-sdk';

import { ApiHandler } from '../../common/api.interfaces';
import { SwaggerHandler } from './swagger.handler';
import { SwaggerRepository } from './swagger.repository';
import { SwaggerService } from './swagger.service';

const defaultRegion: string = <string> (process.env.REGION_NAME || process.env.AWS_REGION);
const apiGateway: APIGateway = new APIGateway({ region: defaultRegion });

const repo: SwaggerRepository = new SwaggerRepository(apiGateway);
const service: SwaggerService = new SwaggerService(repo, process.env);
const handler: SwaggerHandler = new SwaggerHandler(service);

export const getSwaggerJson: ApiHandler = handler.getSwaggerJson;
