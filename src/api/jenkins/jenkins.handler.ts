import { ApiCallback, ApiContext, ApiEvent, ApiHandler } from '../../common/api.interfaces';
import { ResponseBuilder } from '../../common/response-builder';
import { WebsocketService } from '../../websocket/websocket.service';
import { PostBuildResult } from './jenkins.interfaces';

export class JenkinsHandler {
    public constructor(private _websocketService: WebsocketService) {
    }

    public postBuild: ApiHandler = async (event: ApiEvent, context: ApiContext, callback: ApiCallback) => {
        const body: any = JSON.parse(<string> event.body);
        const messageId: string = `${this._websocketService.db.Message.Prefix}${Date.now()}`;
        const channelName: string = body.application ? `${body.application}_Secret` : 'General';

        // Save message in database for later
        await this._websocketService.ddbClient.put({
            TableName: this._websocketService.db.Table,
            Item: {
                [this._websocketService.db.Message.Primary.Key]: `${this._websocketService.db.Channel.Prefix}${channelName}`,
                [this._websocketService.db.Message.Primary.Range]: messageId,
                ConnectionId: 'JENKINS_ID',
                Name: 'JenkinsBuild',
                Content: body.content,
                DateCreated: Date.now()
            }
            }).promise();

        ResponseBuilder.ok<PostBuildResult>({ success: true }, callback);
    }
}
