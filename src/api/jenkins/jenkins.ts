import { ApiHandler } from '../../common/api.interfaces';
import { WebsocketService } from '../../websocket/websocket.service';
import { JenkinsHandler } from './jenkins.handler';

const handler: JenkinsHandler = new JenkinsHandler(new WebsocketService());

export const postBuild: ApiHandler = handler.postBuild;
