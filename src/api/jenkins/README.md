## Jenkins Trigger

```
aws lambda invoke \
--invocation-type RequestResponse \
--function-name otf-notification-dev-jenkins \
--region us-east-1 \
--log-type Tail \
--payload '{"body":"{\"content\":\"Latest version v1.0\", \"application\":\"FCA\"}"}' \
--profile otfDev \
outputfile.txt
```