import { APIGatewayEvent, Context, ProxyCallback, ProxyResult } from 'aws-lambda';  // tslint:disable-line no-implicit-dependencies (Using only the type information from the @types package.)
import { ErrorResult } from './errors';

export interface ApiDynamoEvent extends APIGatewayEvent {
  Records: object[];
}

export type ApiCallback = ProxyCallback;
export type ApiContext = Context;
export type ApiEvent = APIGatewayEvent;
export type ApiHandler = (event: ApiEvent & ApiEvent, context: Context, callback: ApiCallback) => void; // Same as ProxyHandler, but requires callback.
export type ApiDynamoHandler = (event: ApiDynamoEvent & ApiEvent, context: Context, callback: ApiCallback) => void; // Same as ProxyHandler, but requires callback.
export type ApiResponse = ProxyResult;

export interface ErrorResponseBody {
  error: ErrorResult;
}
