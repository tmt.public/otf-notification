import * as AWS from 'aws-sdk';
import { DocumentClient } from 'aws-sdk/clients/dynamodb';

export class WebsocketService {
    public db: any;
    public ddbClient: any;

    public constructor() {
        const ddbVar: DocumentClient = new AWS.DynamoDB.DocumentClient();

        const dbVar: any = {
            Table: process.env.CONNECTION_TABLE,
            Connection: {
                Channels: {
                    Index: 'reverse',
                    Key: 'message_id',
                    Range: 'channel_id'
                },
                Entity: 'CONNECTION',
                Prefix: 'CONNECTION|',
                Primary: {
                    Key: 'channel_id',
                    Range: 'message_id'
                }
            },
            Channel: {
                Connections: {
                    Key: 'channel_id',
                    Range: 'message_id'
                },
                Entity: 'CHANNEL',
                Messages: {
                    Key: 'channel_id',
                    Range: 'message_id'
                },
                Prefix: 'CHANNEL|',
                Primary: {
                    Key: 'channel_id',
                    Range: 'message_id'
                }
            },
            Message: {
                Entity: 'MESSAGE',
                Prefix: 'MESSAGE|',
                Primary: {
                    Key: 'channel_id',
                    Range: 'message_id'
                },
                Columns: {
                    Name: 'Name',
                    Content: 'Content',
                    DateCreated: 'DateCreated'
                }
            },
            Primary: {
                Key: 'channel_id',
                Range: 'message_id'
            }
        };

        this.db = dbVar;
        this.ddbClient = ddbVar;
    }

    public fetchChannelSubscriptions = async (channel: any): Promise<any> => {
        const channelId: string = this.parseEntityId(channel);
        const results: any = await this.ddbClient.query({
            TableName: this.db.Table,
            KeyConditionExpression: `${this.db.Channel.Connections.Key} = :channelId and begins_with(
                ${this.db.Channel.Connections.Range}, :connectionEntity)`,
            ExpressionAttributeValues: {
                ':channelId': `${this.db.Channel.Prefix}${channelId}`,
                ':connectionEntity': this.db.Connection.Prefix
            }
        }).promise();

        return results.Items;
    }

    public fetchConnectionSubscriptions = async (connection: any): Promise<any> => {
            const connectionId: string = this.parseEntityId(connection);
            const results: any = await this.ddbClient.query({
                TableName: this.db.Table,
                IndexName: this.db.Connection.Channels.Index,
                KeyConditionExpression: `${this.db.Connection.Channels.Key} = :connectionId and begins_with(
                    ${this.db.Connection.Channels.Range}, :channelEntity)`,
                ExpressionAttributeValues: {
                    ':connectionId': `${this.db.Connection.Prefix}${connectionId}`,
                    ':channelEntity': this.db.Channel.Prefix
                }
            }).promise();

            return results.Items;
    }

    public parseEntityId = (target: any): string => {
        let out: any;

        if (typeof target === 'object') {
                out = target.requestContext.connectionId;
        } else {
            // Get raw id
            const separatorIndex: number = target.indexOf('|');
            out = target.substring(separatorIndex + 1, target.length);
        }

        return out;
    }
}
