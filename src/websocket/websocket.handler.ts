import { ApiCallback, ApiContext, ApiDynamoEvent, ApiDynamoHandler, ApiEvent, ApiHandler } from '../common/api.interfaces';
import { ResponseBuilder } from '../common/response-builder';
import { WebsocketClient } from './websocket.client';
import { WebsocketService } from './websocket.service';

export class WebsocketHandler {

    public constructor(private _service: WebsocketService, private _wsClient: WebsocketClient) {
    }

    public broadcast: ApiDynamoHandler = async (event: ApiDynamoEvent, context: ApiContext, callback: ApiCallback) => {
        // Broadcast the news
        const results: any = event.Records.map(async (record: any) => {
        // Connection was created or dropped
        const channelId: string = this._service.parseEntityId(
            record.dynamodb.Keys[this._service.db.Primary.Key].S
        );

        const subscribers: any[] = await this._service.fetchChannelSubscriptions(channelId);

        /* 3 types of keys
         - Connection
         - Channel
         - Message
        */
        switch (record.dynamodb.Keys[this._service.db.Primary.Key].S.split('|')[0]) {
            // Connection entities
            case this._service.db.Connection.Entity:
                break;
            case this._service.db.Channel.Entity:
                switch (record.dynamodb.Keys[this._service.db.Primary.Range].S.split('|')[0]) {
                    case this._service.db.Connection.Entity: {
                        let eventType: string = 'sub';
                        if (record.eventName === 'REMOVE') {
                            eventType = 'unsub';
                        } else if (record.eventName === 'UPDATE') {
                            break;
                        }

                        const results: any = subscribers.map(async (subscriber: any) => {
                            const subscriberId: string = this._service.parseEntityId(
                            subscriber[this._service.db.Channel.Connections.Range]
                            );
                            return this._wsClient.send(
                            subscriberId,
                            {
                                event: `subscriber_${eventType}`,
                                channelId,

                                // Sender id
                                subscriberId: this._service.parseEntityId(
                                    record.dynamodb.Keys[this._service.db.Primary.Range].S
                                )
                            }
                            );
                        });

                        await Promise.all(results);
                        break;
                        }

                    // New message
                    case this._service.db.Message.Entity: {
                        if (record.eventName !== 'INSERT') {
                            ResponseBuilder.ok<object>({}, callback);
                        }
                        const results: any = subscribers.map(async (subscriber: any) => {
                            const subscriberId: string = this._service.parseEntityId(
                            subscriber[this._service.db.Channel.Connections.Range]
                            );

                            const dateCreated: any = record.dynamodb.NewImage[this._service.db.Message.Columns.DateCreated];

                            return this._wsClient.send(subscriberId, {
                                event: 'channel_message',
                                channelId,
                                name: record.dynamodb.NewImage[this._service.db.Message.Columns.Name].S,
                                content: record.dynamodb.NewImage[this._service.db.Message.Columns.Content].S,
                                dateCreated: dateCreated ? dateCreated.N : Date.now()
                            });
                        });

                        await Promise.all(results);
                        break;
                    }
                    default:
                }

                break;
                default:
            }
        });

        await Promise.all(results);
        ResponseBuilder.ok<object>({}, callback);
    }

    public channelManager: ApiHandler = (event: ApiEvent, context: ApiContext, callback: ApiCallback) => {
        const action: string = JSON.parse(<string> event.body).action;
        switch (action) {
        case 'subscribeChannel':
            this.subscribeChannel(event, context, callback);
            break;
        case 'unsubscribeChannel':
            this.unsubscribeChannel(event, context, callback);
            break;
        default:
        }

        ResponseBuilder.ok<object>({}, callback);
    }

    public connectionManager: ApiHandler = async (event: any, context: ApiContext, callback: ApiCallback) => {
            await this._wsClient._setupClient(event);

            if (event.requestContext.eventType === 'CONNECT') {
                // Sub general channel
                this.subscribeChannel(
                {
                    ...event,
                    body: JSON.stringify({
                    action: 'subscribe',
                    channelId: 'General'
                    })
                },
                context, callback
                );

                ResponseBuilder.ok<object>({}, callback);
            } else if (event.requestContext.eventType === 'DISCONNECT') {
                // Unsub all channels connection was in
                const subscriptions: any = await this._service.fetchConnectionSubscriptions(event);
                const unsubscribes: any = subscriptions.map(async (subscription: any) =>
                this.unsubscribeChannel(
                    {
                    ...event,
                    body: JSON.stringify({
                        action: 'unsubscribe',
                        channelId: this._service.parseEntityId(subscription[this._service.db.Channel.Primary.Key])
                    })
                    },
                    context, callback
                )
                );

                await Promise.all(unsubscribes);
                ResponseBuilder.ok<object>({}, callback);
            }
        }

    public defaultMessage: ApiHandler = async (event: ApiEvent, context: ApiContext, callback: ApiCallback) => {
        await this._wsClient.send(event, {
        event: 'error',
        message: 'invalid action type'
        });

        ResponseBuilder.ok<object>({}, callback);
    }

    public sendMessage: ApiHandler = async (event: any, context: ApiContext, callback: ApiCallback) => {
        const body: any = JSON.parse(<string> event.body);
        const messageId: string = `${this._service.db.Message.Prefix}${Date.now()}`;
        const name: string = body.name
            .replace(/[^a-z0-9\s-]/gi, '')
            .trim()
            .replace(/\+s/g, '-');

        // Save message in database for later
        await this._service.ddbClient.put({
        TableName: this._service.db.Table,
        Item: {
            [this._service.db.Message.Primary.Key]: `${this._service.db.Channel.Prefix}${body.channelId}`,
            [this._service.db.Message.Primary.Range]: messageId,
            ConnectionId: `${event.requestContext.connectionId}`,
            Name: name,
            Content: body.content,
            DateCreated: Date.now()
        }
        }).promise();

        ResponseBuilder.ok<object>({}, callback);
    }

    public subscribeChannel: ApiHandler = async (event: ApiEvent, context: ApiContext, callback: ApiCallback) => {
        const channelId: string = JSON.parse(<string> event.body).channelId;
        await this._service.ddbClient.put({
        TableName: this._service.db.Table,
        Item: {
            [this._service.db.Channel.Connections.Key]: `${this._service.db.Channel.Prefix}${channelId}`,
            [this._service.db.Channel.Connections.Range]: `${this._service.db.Connection.Prefix}${
            this._service.parseEntityId(event)
            }`
        }
        }).promise();
        ResponseBuilder.ok<object>({}, callback);
    }

    public unsubscribeChannel: ApiHandler = async (event: ApiEvent, context: ApiContext, callback: ApiCallback) => {
            const channelId: string = JSON.parse(<string> event.body).channelId;
            await this._service.ddbClient.delete({
            TableName: this._service.db.Table,
            Key: {
                [this._service.db.Channel.Connections.Key]: `${this._service.db.Channel.Prefix}${channelId}`,
                [this._service.db.Channel.Connections.Range]: `${this._service.db.Connection.Prefix}${
                this._service.parseEntityId(event)
                }`
            }
            }).promise();
            ResponseBuilder.ok<object>({}, callback);
        }
}
