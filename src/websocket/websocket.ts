import { ApiDynamoHandler, ApiHandler } from '../common/api.interfaces';
import { WebsocketClient } from './websocket.client';
import { WebsocketHandler } from './websocket.handler';
import { WebsocketService } from './websocket.service';

const wsService: WebsocketService = new WebsocketService();
const handler: WebsocketHandler = new WebsocketHandler(wsService, new WebsocketClient(wsService));

export const connectionManager: ApiHandler = handler.connectionManager;
export const broadcast: ApiDynamoHandler = handler.broadcast;
export const sendMessage: ApiHandler = handler.sendMessage;
export const defaultMessage: ApiHandler = handler.defaultMessage;
export const channelManager: ApiHandler = handler.channelManager;
