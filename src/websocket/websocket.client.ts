import * as AWS from 'aws-sdk';
import 'aws-sdk/clients/apigatewaymanagementapi';
import * as Bluebird from 'bluebird';
import { HttpStatusCode } from '../common/http-status-codes';
import { WebsocketService } from './websocket.service';

AWS.config.setPromisesDependency(Bluebird);

export class WebsocketClient {
    private client: any;

    public constructor(private readonly _service: WebsocketService, config?: any) {
        if (config) {
            this._setupClient(config);
        }
    }

    public _setupClient = async (config: any): Promise<any> => {
        // Fetch config from db
        if (typeof config !== 'object' && !this.client) {
            const item: any = await this._service.ddbClient.get({
                TableName: this._service.db.Table,
                Key: {
                    [this._service.db.Primary.Key]: 'NOTIFICATION',
                    [this._service.db.Primary.Range]: 'WS_CONFIG'
                }
            }).promise();
            config = item.Item;
            if (config) {
                config.fromDb = true;
            }
        }

        if (!this.client) {

            if (config.requestContext.apiId) {
                config.requestContext.domainName  = `${config.requestContext.apiId}.execute-api.${process.env.API_REGION}.amazonaws.com`;
            }

            this.client = new AWS.ApiGatewayManagementApi({
                apiVersion: '2018-11-29',
                endpoint: `https://${config.requestContext.domainName}/${config.requestContext.stage}`
            });

            if (config.fromDb !== true) {
                await this._service.ddbClient.put({
                    TableName: this._service.db.Table,
                    Item: {
                        [this._service.db.Primary.Key]: 'NOTIFICATION',
                        [this._service.db.Primary.Range]: 'WS_CONFIG',
                        requestContext: {
                            domainName: config.requestContext.domainName,
                            stage: config.requestContext.stage
                        }
                    }
                }).promise();
            }
        }
    }

    public send = async (connection: any, payload: any): Promise<any> => {
        await this._setupClient(connection);

        let connectionId: any = connection;
        if (typeof connection === 'object') {
            connectionId = connection.requestContext.connectionId;
        }

        await this.client.postToConnection({
            ConnectionId: connectionId,
            Data: JSON.stringify(payload)
        }).promise().catch(async (err: any) => {

            if (err.statusCode === HttpStatusCode.PermanentlyDeleted) {
                // Unsub all channels connection was in
                const subscriptions: any = await this._service.fetchConnectionSubscriptions(connectionId);
                const unsubscribes: any = subscriptions.map(async (subscription: any) =>
                    this._service.ddbClient.delete({
                        TableName: this._service.db.Table,
                        Key: {
                            [this._service.db.Channel.Connections.Key]: `${this._service.db.Channel.Prefix}${this._service.parseEntityId(subscription[this._service.db.Channel.Primary.Key])}`,
                            [this._service.db.Channel.Connections.Range]: `${this._service.db.Connection.Prefix}${connectionId}`
                        }
                    }).promise()
                );

                await Promise.all(unsubscribes);
            }
        });

        return true;
    }
}
