
### Messages

`{"action": "sendMessage", "name": "Mann", "channelId": "General", "content": "hello!"}`

### Channel Subscriptions

`{"action": "subscribeChannel", "channelId": "Secret"}`
`{"action": "unsubscribeChannel", "channelId": "Secret"}`
